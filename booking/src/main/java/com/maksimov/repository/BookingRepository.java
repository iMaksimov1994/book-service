package com.maksimov.repository;


import com.maksimov.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    void deleteBookingByStatusAndCreatedLessThan(Booking.Status status, Instant created);

    @Query("SELECT b FROM Booking b " +
            " WHERE b.status!= 'CANCELED' AND b.status!='ACCEPTED'" +
            "AND b.created<:created")
    List<Booking> bookingsForCancelingByLifeTime(Instant created);

    Optional<Booking> findBookingByTittleAndUserName(String tittle, String userName);

    Optional<Booking> findBookingById(Long id);
}
