package com.maksimov.controller;

import com.maksimov.dto.BookingDTO;
import com.maksimov.service.BookingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/booking")
@RequiredArgsConstructor
public class BookingController {
    private final BookingService bookingService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookingDTO createBooking(@RequestBody BookingDTO bookingDTO) {
        return bookingService.createBooking(bookingDTO);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public BookingDTO acceptBooking(@RequestParam Long bookId) {
        return bookingService.acceptBooking(bookId);
    }
}
