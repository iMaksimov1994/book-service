package com.maksimov.model;


import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import java.time.Instant;

@Entity
@Table(name = "booking")
@Data
@EqualsAndHashCode(of = "id")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tittle", nullable = false)
    private String tittle;

    @Column(name = "quantity", nullable = false)
    private int quantity;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Instant created;

    @Enumerated(EnumType.STRING)
    private Status status;

    public enum Status {
        CREATED,
        CONFIRMED,
        ACCEPTED,
        CANCELED
    }
}
