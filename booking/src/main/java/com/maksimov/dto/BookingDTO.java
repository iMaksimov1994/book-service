package com.maksimov.dto;


import com.maksimov.model.Booking;
import lombok.*;


import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookingDTO {
    private String tittle;
    private int quantity;
    private String userName;
    private Instant created;
    private Booking.Status status;
}
