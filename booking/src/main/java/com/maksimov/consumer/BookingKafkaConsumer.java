package com.maksimov.consumer;


import com.maksimov.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.maksimov.dto.BookKafkaDTO;
import com.maksimov.model.Booking;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingKafkaConsumer {
    private final BookingRepository bookingRepository;

    @KafkaListener(topics = "${spring.kafka.book}", groupId = "${spring.kafka.group_id}")
    public void consume(BookKafkaDTO bookKafkaDTO) {
        if (bookKafkaDTO.getStatus() == BookKafkaDTO.Status.CONFIRMED) {
            log.info("BookKafkaDTO from book service CONFIRMED {}", bookKafkaDTO);
            Optional<Booking> bookingById = bookingRepository.
                    findBookingById(bookKafkaDTO.getBookingId());

            if (bookingById.isPresent()) {
                Booking booking = bookingById.get();
                log.info("Booking from database correct {}", booking);

                if (booking.getStatus() != Booking.Status.CANCELED
                        & booking.getStatus() != Booking.Status.CONFIRMED) {
                    booking.setStatus(Booking.Status.CONFIRMED);
                    bookingRepository.save(booking);
                }
            }
        } else {
            log.error("BookKafkaDTO from book service NOT_CONFIRMED {}", bookKafkaDTO);
        }
    }
}
