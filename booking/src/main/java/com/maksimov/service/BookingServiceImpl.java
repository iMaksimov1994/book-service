package com.maksimov.service;


import com.maksimov.dto.BookKafkaDTO;
import com.maksimov.dto.BookingDTO;
import com.maksimov.model.Booking;
import com.maksimov.exceptions.EntityAlreadyExist;
import com.maksimov.exceptions.EntityNotExistOrNotFound;
import com.maksimov.mapper.BookingMapper;
import com.maksimov.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class BookingServiceImpl implements BookingService {
    @Value("${spring.kafka.booking}")
    private String topic;
    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;
    private final KafkaTemplate<String, BookKafkaDTO> kafkaTemplate;

    @Override
    @Transactional
    public BookingDTO createBooking(BookingDTO bookingDTO) {
        Optional<Booking> bookingByTittleAndUserName =
                bookingRepository.findBookingByTittleAndUserName(bookingDTO.getTittle(),
                        bookingDTO.getUserName());
        if (bookingByTittleAndUserName.isPresent()) {
            log.info("Booking already exist!");
            throw new EntityAlreadyExist("Booking already exist!");
        } else {
            Booking booking = bookingMapper.toBooking(bookingDTO);
            booking.setStatus(Booking.Status.CREATED);
            Booking saveBooking = bookingRepository.save(booking);
            log.info("Save booking in database correctly:{}", saveBooking);

            BookKafkaDTO bookKafkaDTO = BookKafkaDTO.builder()
                    .tittle(saveBooking.getTittle())
                    .quantity(saveBooking.getQuantity())
                    .bookingId(saveBooking.getId())
                    .build();
            kafkaTemplate.send(topic, bookKafkaDTO);
            log.info("Send bookKafkaDTO from booking to book correctly:{}", bookKafkaDTO);
            return bookingMapper.toBookingDTO(saveBooking);
        }
    }

    @Transactional
    @Override
    public BookingDTO acceptBooking(Long bookingId) {
        Optional<Booking> bookingByTittleAndUserName =
                bookingRepository.findBookingById(bookingId);
        if (bookingByTittleAndUserName.isEmpty()) {
            log.info("Booking not exist!");
            throw new EntityNotExistOrNotFound("Booking not exist!");
        } else {
            Booking booking = bookingByTittleAndUserName.get();
            Booking.Status status = booking.getStatus();
            if (status == Booking.Status.CONFIRMED) {
                booking.setStatus(Booking.Status.ACCEPTED);
                Booking updateBooking = bookingRepository.save(booking);
                return bookingMapper.toBookingDTO(updateBooking);
            } else {
                log.warn("Booking already ACCEPTED or NOT ACCEPTED or CANCELED!");
                throw new IllegalArgumentException("Booking has incorrect status!");
            }
        }
    }
}
