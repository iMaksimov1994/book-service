package com.maksimov.service;


import com.maksimov.dto.BookingDTO;

public interface BookingService {

    BookingDTO createBooking(BookingDTO bookingDTO);

    BookingDTO acceptBooking(Long bookingId);
}
