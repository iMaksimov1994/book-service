package com.maksimov.schedulers;

import com.maksimov.dto.BookKafkaDTO;
import com.maksimov.model.Booking;
import com.maksimov.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class BookingSchedulers {
    @Value("${spring.kafka.booking}")
    private String topic;
    @Value("${scheduler.millsToSubtract}")
    private long millsToSubtract;
    private final BookingRepository bookingRepository;
    private final KafkaTemplate<String, BookKafkaDTO> kafkaTemplate;

    @Transactional
    @Scheduled(fixedDelayString = "${scheduler.timePeriodForCanceledBooking}")
    public void canceledBookingBySchedule() {
        Instant now = Instant.now();
        Instant bookingEndOfLifeCycle = now.minusMillis(millsToSubtract);
        List<Booking> bookingDTOS =
                bookingRepository.bookingsForCancelingByLifeTime(bookingEndOfLifeCycle);

        bookingDTOS.forEach(x -> x.setStatus(Booking.Status.CANCELED));

        bookingDTOS.forEach(x ->
                kafkaTemplate.send(topic, BookKafkaDTO.builder()
                        .tittle(x.getTittle())
                        .bookingId(x.getId())
                        .status(BookKafkaDTO.Status.RETURNED)
                        .quantity(x.getQuantity())
                        .build()));
    }
}