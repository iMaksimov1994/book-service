package com.maksimov.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maksimov.dto.BookKafkaDTO;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonSerializer;


@Configuration
public class KafkaConsumerConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    @Bean
    public ConsumerFactory<String, BookKafkaDTO> consumerFactory(KafkaProperties kafkaProperties, ObjectMapper objectMapper) {
        var consumerProperties = kafkaProperties.buildConsumerProperties();
        consumerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        consumerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        consumerProperties.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, 5000);
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        var kafkaConsumerFactory =
                new DefaultKafkaConsumerFactory<String, BookKafkaDTO>(consumerProperties);
        kafkaConsumerFactory.setValueDeserializer(new org.springframework.kafka.support.
                serializer.JsonDeserializer<>(objectMapper));
        return kafkaConsumerFactory;
    }

    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, BookKafkaDTO>>
    kafkaListenerContainerFactory(ConsumerFactory<String, BookKafkaDTO>
                                          consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, BookKafkaDTO> concurrentMessageListenerContainer =
                new ConcurrentKafkaListenerContainerFactory<>();
        concurrentMessageListenerContainer.setConsumerFactory(consumerFactory);
        return concurrentMessageListenerContainer;
    }
}
