package com.maksimov.mapper;


import com.maksimov.dto.BookingDTO;
import com.maksimov.model.Booking;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    BookingDTO toBookingDTO(Booking booking);

    Booking toBooking(BookingDTO bookingDTO);
}
