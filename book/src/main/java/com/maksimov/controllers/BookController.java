package com.maksimov.controllers;


import com.maksimov.dto.BookDTO;
import com.maksimov.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookDTO addBook(@RequestBody BookDTO bookDTO) {
        return bookService.addBook(bookDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public BookDTO getQuantityBookByTittle(@RequestParam String tittle) {
        return bookService.getQuantityBookByTittle(tittle);
    }
}
