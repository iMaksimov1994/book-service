package com.maksimov.mappers;


import com.maksimov.dto.AuthorDTO;
import com.maksimov.model.Author;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthorMapper {
    AuthorDTO toAuthorDTO(Author author);

    Author fromAuthorDTO(AuthorDTO authorDTO);
}
