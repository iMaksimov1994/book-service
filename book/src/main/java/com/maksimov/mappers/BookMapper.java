package com.maksimov.mappers;



import com.maksimov.dto.BookDTO;
import com.maksimov.model.Book;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookMapper {
    BookDTO toBookDTO(Book book);

    Book fromBookDTO(BookDTO bookDTO);
}

