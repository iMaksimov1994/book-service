package com.maksimov.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthorDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 5509320186360689663L;
    private String name;
    private String lastName;
}