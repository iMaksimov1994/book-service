package com.maksimov.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookKafkaDTO {
    private String tittle;
    private int quantity;
    private long bookingId;
    private Status status;

    public enum Status {
        CONFIRMED,
        RETURNED
    }
}
