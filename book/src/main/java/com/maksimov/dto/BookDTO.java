package com.maksimov.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 8891446274971474160L;
    private String tittle;
    private int quantity;
    private List<AuthorDTO> authors;
}