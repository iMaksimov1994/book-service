package com.maksimov.consumer;

import com.maksimov.dto.BookDTO;
import com.maksimov.dto.BookKafkaDTO;
import com.maksimov.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookKafkaConsumer {
    private final BookService bookService;

    @KafkaListener(topics = "${spring.kafka.booking}", groupId = "${spring.kafka.group_id}")
    public void consume(BookKafkaDTO bookKafkaDTO) {
        log.info("message consumed {}", bookKafkaDTO);

        try {
            BookDTO bookDTO = bookService.updateBook(bookKafkaDTO);
            log.info("Book correctly update in database:{}", bookDTO);

        } catch (Exception e) {
            log.error("Can't correctly update quantity book in database:{}", e.getMessage());
        }
    }
}
