package com.maksimov.service;


import com.maksimov.mappers.BookMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.maksimov.dto.AuthorDTO;
import com.maksimov.dto.BookDTO;
import com.maksimov.dto.BookKafkaDTO;
import com.maksimov.exceptions.EntityAlreadyExist;
import com.maksimov.exceptions.EntityNotExistOrNotFound;
import com.maksimov.mappers.AuthorMapper;
import com.maksimov.model.Author;
import com.maksimov.model.Book;
import com.maksimov.repository.AuthorRepository;
import com.maksimov.repository.BookRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;
    @Value("${spring.kafka.book}")
    private String topic;
    private final KafkaTemplate<String, BookKafkaDTO> template;

    @Override
    @Transactional
    @Retryable(maxAttempts = 2)
    public BookDTO addBook(BookDTO bookDTO) {
        Optional<Book> bookByTittleIgnoreCase = bookRepository.getBookByTittleIgnoreCase(bookDTO.getTittle());
        if (bookByTittleIgnoreCase.isPresent()) {
            log.info("Book already exist!");
            throw new EntityAlreadyExist("Book already exist!");
        } else {
            List<AuthorDTO> authorDTOs = bookDTO.getAuthors();
            List<Author> authors = authorDTOs.stream().map(authorMapper::fromAuthorDTO).toList();

            List<Author> authorsInDB = new ArrayList<>(authors.stream().map(x ->
                    this.authorRepository.findAuthorByNameAndLastNameIgnoreCase(x.getName(),
                            x.getLastName())).filter(Optional::isPresent).map(Optional::get).toList());

            List<Author> originalAuthors = authors.stream().filter(x ->
                            this.authorRepository.findAuthorByNameAndLastNameIgnoreCase(x.getName(),
                                    x.getLastName()).isEmpty()).
                    toList();
            authorsInDB.addAll(originalAuthors);
            Book book = bookMapper.fromBookDTO(bookDTO);
            book.setAuthors(authorsInDB);


            Book saveBook = this.bookRepository.save(book);
            log.info("Save book in database correctly:{}", saveBook);
            return bookMapper.toBookDTO(saveBook);
        }
    }

    @Override
    @Cacheable(cacheNames = "quantityBook")
    public BookDTO getQuantityBookByTittle(String tittle) {
        Book book = bookRepository.getBookByTittleIgnoreCase(tittle).
                orElseThrow(() ->
                        new EntityNotExistOrNotFound("Book not found by this tittle!"));
        log.info("Get book from database correctly by tittle:{}", book);
        int quantity = book.getQuantity();
        log.info("Get quantity of books with the same name:{}", quantity);
        return bookMapper.toBookDTO(book);
    }

    @Override
    @Transactional
    @CachePut(cacheNames = "quantityBook")
    public BookDTO updateBook(BookKafkaDTO bookKafkaDTO) {
        int quantity = bookKafkaDTO.getQuantity();
        String tittle = bookKafkaDTO.getTittle();
        Book book = bookRepository.getBookByTittleIgnoreCase(tittle).
                orElseThrow(() ->
                        new EntityNotExistOrNotFound("Book not found by tittle!"));
        log.info("Get book from database correctly by tittle:{}", book);
        int quantityOfBookInDB = book.getQuantity();
        if (bookKafkaDTO.getStatus() != BookKafkaDTO.Status.RETURNED) {
            if (quantityOfBookInDB < quantity) {
                log.info("Quantity of book not enough!:{}", book);
                throw new IllegalArgumentException("Quantity of book not enough!");
            }
            book.setQuantity(quantityOfBookInDB - quantity);
            bookKafkaDTO.setStatus(BookKafkaDTO.Status.CONFIRMED);
            template.send(topic, bookKafkaDTO);
        } else {
            book.setQuantity(quantityOfBookInDB + quantity);
        }
        log.info("Update quantity of book correctly");
        return bookMapper.toBookDTO(bookRepository.save(book));
    }
}