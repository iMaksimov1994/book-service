package com.maksimov.service;


import com.maksimov.dto.BookDTO;
import com.maksimov.dto.BookKafkaDTO;

public interface BookService {
    BookDTO addBook(BookDTO bookDTO);

    BookDTO getQuantityBookByTittle(String tittle);

    BookDTO updateBook(BookKafkaDTO bookKafkaDTO);
}
